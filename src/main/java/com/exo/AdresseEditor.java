package com.exo;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class AdresseEditor extends PropertiesEditor {
	@Override
    public String getAsText() {		
		Adresse adresse = (Adresse) getValue();		
		return adresse.getAdresse() + ","+ adresse.getCodePostal() + "," + adresse.getVille() + "," + adresse.getPays();
    }
    
	 @Override
	    public void setAsText(String text) throws IllegalArgumentException {		 
		 if(text.equals("")) {			
			 setValue(null);
		 }
		 else {
			 String[] arrStrings = text.split(",");
						 
			 Adresse adr = new Adresse();			 
			adr.setAdresse(arrStrings[0]);
			adr.setCodePostal(arrStrings[1]);
			adr.setVille(arrStrings[2]);
		     adr.setPays(arrStrings[3]);			 
			 setValue(adr);
		 }
		 
	    }
}








//if(text.equals("")) {
//	 setValue(null);
//}
//else {
//	 String[] listeStr = text.split(",");
//	 
//	 Adresse adresse = new Adresse();
//	 
//	 adresse.setAdresse(listeStr[0]);
//	 adresse.setCodePostal(listeStr[1]);
//	 adresse.setVille(listeStr[2]);
//	 adresse.setPays(listeStr[3]);
//	 setValue(adresse);
//}	 