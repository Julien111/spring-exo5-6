package com.exo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Client {
	
	private String nom;
	
	private String prenom;
	
	private int age;
	
	private List<Adresse> adresses;
	
	private static final Log LOG = LogFactory.getLog(Client.class);
	
	public Client() {
		super();
		if (LOG.isDebugEnabled()) {
			 LOG.debug("-- on est dans le constructeur " + this.getClass().getName());
		 }
	}
	
	@PostConstruct
	public void init() {
		if (LOG.isDebugEnabled()) {
			 LOG.debug("-- initialisation de l'objet client " + this.getNom());
			 }
	}
	
	@PreDestroy
	public void destroy() {
		if (LOG.isDebugEnabled()) {
			 LOG.debug("-- destruction de l'objet client " + this.getNom());
			 }
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}	
	
	public List<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}
	
	@Override
	public String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", les adresses : " + adresses.toString() + "]";
	}	
	
}
